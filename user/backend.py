#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2021

"""
backend for user app
"""

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.hashers import check_password
from .models import User, Code


class PhoneUserBackend(ModelBackend):
    """
    django user backend to login with phone number
    """
    def authenticate(
            self, request, username=None, password=None, *args, **kwargs):
        if 'phone' in kwargs.keys():
            phone = kwargs['phone']
        else:
            phone = username
        try:
            user = User.objects.get(phone=phone)
        except User.DoesNotExist:
            return None
        if 'code' in kwargs.keys():
            code = kwargs['code']
            return sign_with_code(user, code)
        return sign_with_pass(user, password)

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


def sign_with_pass(user, password):
    """
    if user have no otp code, let's try password
    """
    if check_password(password, user.password):
        if user.is_active:
            return user
    return None


def sign_with_code(user, code):
    """
    when there is a 'code' key in keyword arguments
    """
    obj = Code.objects.filter(user=user).order_by('-date')
    try:
        number = obj[0].code
        obj.delete()
    except IndexError:
        return None
    if code == number:
        return user
    return None
