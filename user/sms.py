#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2021

"""
sms tool for user app
"""

import logging
from random import randint

import kavenegar

from beru.settings import SMS_API_KEY
from .models import Code

logger = logging.getLogger(__name__)


def sms_code(user):
    """
    sends the sms to user
    """
    code = create_code(user)
    # sms from here
    try:
        api = kavenegar.KavenegarAPI(SMS_API_KEY)
        params = {
            'receptor': str(user.phone),
            'template': 'verify',
            'token': code.code,
            'type': 'sms',  # sms vs call
              }
        response = api.verify_lookup(params)
        logger.warning(response)
    except kavenegar.APIException as exception:
        logger.error(exception)
    except kavenegar.HTTPException as exception:
        logger.error(exception)
    logger.warning(str(user.phone) + ': ' + str(code.code))
    # sms to here
    return True


def create_code(user):
    """
    returns a Code model object
    """
    number = create_code_number(6)
    code = Code(user=user, code=number)
    code.save()
    return code


def create_code_number(size):
    """
    creates an otp code of size 'size'
    """
    return str(randint(10**(size-1), 10**size-1))
