#!/usr/bin/python3
# Released under GPLv3+ License
# Javad Nikbakht <javadnikbakht@mail.com>, 2021

"""
froms for user app
"""

from django import forms
from .models import User


class GetUsernameForm(forms.ModelForm):
    """
    form to get username from user
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = "نام کاربری"

    class Meta:
        """
        Meta data for class
        """
        model = User
        fields = ('username',)
        labels = {"username": ""}
