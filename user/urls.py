#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2021

from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('send', views.send_code, name="send"),
    path('login', views.login, name="login"),
    path('check_otp', views.check_otp, name="check_otp"),
    path('set_username', views.set_username, name="set_username"),
    path('', views.login, name="main-page"),
    ]
