"""
views for user app
"""
import random
import re
import string
from urllib.parse import quote_plus as quote

from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.http import HttpResponse

import phonenumbers as p
from phonenumber_field import validators

from beru.settings import DEFAULT_REGION as region
from .models import User
from .sms import sms_code
from .forms import GetUsernameForm

# Create your views here.


def convert_get(request, get_in=None):
    """
    Gets GET from request and returns it as a string.
        If there is an additional argument, appends it.
    """
    if get_in is None:
        get_in = {}
    for item in request.GET:
        get_in.update({item: request.GET[item]})
    get_out = '&'.join('{}={}'.format(
        quote(item), quote(get_in[item])) for item in get_in)
    if get_out.endswith("%2F"):
        get_out = get_out[:-3]
    return get_out


def login(request):
    """
    login url view
    """
    template = 'user/login.html'
    get = convert_get(request)
    context = {'post_url': 'send?{}'.format(get)}
    return render(request, template, context)


def send_code(request):
    """
    send url view
    """
    phone = get_phone(request)
    user = get_user(phone)
    if sms_code(user):
        get_in = {'phone': phone}
        get = convert_get(request, get_in)
        if user.first_login:
            return redirect('check_otp?{}/'.format(get))
        return redirect('cas/login?{}'.format(get))
    return redirect('cas/login')


def get_phone(request):
    """
    returns phone number from request
    """
    if request.method == "POST" and 'phone' in request.POST:
        phone = request.POST.get('phone', '')
    else:
        phone = request.GET.get('phone')
    phone = p.format_number(p.parse(phone, region), p.PhoneNumberFormat.E164)
    if not is_valid(phone):
        return HttpResponse('Send a valid phone number', status=400)
    return phone


def get_user(phone):
    """
    returns user from phone number
    """
    try:
        user = User.objects.get(phone=phone)
    except User.DoesNotExist:
        username = generate_username()
        user = User.objects.create_user(phone=phone, username=username,)
    return user


def generate_username():
    """
    generates a random username
    """
    alnum = string.ascii_lowercase+string.digits
    while True:
        username = ''.join(random.choices(alnum, k=20))
        if username.isdigit():
            continue
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            break
    return username


def is_valid(phone):
    """
    checks given phone number against 'phonenumbers' library
    """
    try:
        validators.validate_international_phonenumber(phone)
    except ValidationError:
        return False
    return True


def check_otp(request):
    """
    check_otp url view
    """
    if request.method == "POST":
        phone = get_phone(request)
        user = get_user(phone)
        code = request.POST.get('password')
        user_authenticated = authenticate(phone=user.phone, code=code)
        get = convert_get(request)
        if user_authenticated and user.first_login:
            return redirect('set_username?{}/'.format(get))
        return redirect('/cas/login')
    return render(request, 'user/get_otp.html')


def set_username(request):
    """
    set_username url view
    """
    phone = get_phone(request)
    user = get_user(phone)
    if request.method == 'POST':
        form = GetUsernameForm(request.POST, instance=user)
        if form.is_valid():
            form_data = form.save(commit=False)
            form_data.user = request.user
            form_data.save()
            user.first_login = False
            user.save()
            get = convert_get(request)
            if sms_code(user):
                return redirect('/cas/login?{}'.format(get))
            phone_pattern = r"phone=%2B?\d{12}"
            get = re.sub(phone_pattern, '', get)
            return redirect('/cas/login?{}'.format(get))
        form = GetUsernameForm()
        repeated_username_msg = "نام کاربری وارد شده صحیح نیست"
        context = {
            'form': form,
            'bad_msg': repeated_username_msg,
        }
        return render(request, 'user/set_username.html', context)
    form = GetUsernameForm()
    return render(request, 'user/set_username.html', {'form': form, })
