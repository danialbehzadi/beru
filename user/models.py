"""
models for user app
"""
from django.db import models
from django.contrib.auth.models import UserManager, AbstractUser
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.


class PhoneUserManager(UserManager):
    """
    django usermanager class for loging in with phone number
    """
    def create_user(self, phone, username=None, password=None):
        user = self.model(phone=phone)
        if username:
            user.username = username
        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, phone, password):
        user = self.create_user(
              phone=phone,
              password=password,
               )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    """
    user class of user app
    """
    username = models.CharField(max_length=128, blank=True, unique=True)
    phone = PhoneNumberField(null=False, blank=False, unique=True)
    first_login = models.BooleanField(null=False, blank=False, default=True)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []
    objects = PhoneUserManager()

    def __str__(self):
        return str(self.phone)

    class Meta():
        """
        meta data fro user class
        """
        db_table = 'auth_user'
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class Code(models.Model):
    """
    the otp code
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=6)
    date = models.DateTimeField(default=timezone.now)
