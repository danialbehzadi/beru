Beru is a simple Single Sign On (SSO) solution working with phone numbers.

Install
=======
Beru is based on Django. Yo can install it via:
```
$ git clone https://framagit.org/danialbehzadi/beru.git
$ cd beru
$ python3 -m venv .env
$ source .env/bin/activate
(.env)$ pip install -r requirements.txt
(.env)$ ./manage.py migrate
(.env)$ ./manage.py createsuperuser
```

Config
======
If you are using the same mechanism as us for SMS (We are using kavenegar API), change this in your `settings.py`:
```
SMS_API_KEY = 'YOUR API KEY'
```
