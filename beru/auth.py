# -*- coding: utf-8 -*-
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License version 3 for
# more details.
#
# You should have received a copy of the GNU General Public License version 3
# along with this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# (c) 2015-2016 Valentin Samir
"""Some authentication classes for the CAS"""
from django.contrib.auth import get_user_model, authenticate


class AuthUser(object):
    """
        Authentication base class

        :param unicode username: A username, stored in the :attr:`username` class attribute.
    """

    #: username used to instanciate the current object
    username = None

    def __init__(self, username):
        self.username = username

    def test_password(self, password):
        """
            Tests ``password`` against the user-supplied password.

            :raises NotImplementedError: always. The method need to be implemented by subclasses
        """
        raise NotImplementedError()

    def attributs(self):
        """
            The user attributes.

            raises NotImplementedError: always. The method need to be implemented by subclasses
        """
        raise NotImplementedError()


class PhoneAuthUser(AuthUser):  # pragma: no cover
    """
        A django auth class: authenticate user against django internal users

        :param unicode username: A username, stored in the :attr:`username<AuthUser.username>`
            class attribute. Valid value are usernames of django internal users.
    """
    #: a django user object if the username is found. The user model is retreived
    #: using :func:`django.contrib.auth.get_user_model`.
    user = None

    def __init__(self, phone):
        User = get_user_model()
        try:
            self.user = User.objects.get(phone=phone)
        except User.DoesNotExist:
            pass
        super(PhoneAuthUser, self).__init__(phone)

    def test_password(self, password):
        """
            Tests ``password`` against the user-supplied password.

            :param unicode password: a clear text password as submited by the user.
            :return: ``True`` if :attr:`user` is valid and ``password`` is
                correct, ``False`` otherwise.
            :rtype: bool
        """
        self.user = authenticate('request', phone=self.user.phone, code=password)
        if self.user:
            return True
        else:
            return False

    def attributs(self):
        """
            The user attributes, defined as the fields on the :attr:`user` object.

            :return: a :class:`dict` with the :attr:`user` object fields. Attributes may be
                If the user do not exists, the returned :class:`dict` is empty.
            :rtype: dict
        """
        if self.user:
            attr = {}
            # _meta.get_fields() is from the new documented _meta interface in django 1.8
            try:
                field_names = [
                    field.attname for field in self.user._meta.get_fields()
                    if hasattr(field, "attname")
                ]
            # backward compatibility with django 1.7
            except AttributeError:  # pragma: no cover (only used by django 1.7)
                field_names = self.user._meta.get_all_field_names()
            for name in field_names:
                attr[name] = getattr(self.user, name)

            # unfold user_permissions many to many relation
            if 'user_permissions' in attr:
                attr['user_permissions'] = [
                    (
                        u"%s.%s" % (
                            perm.content_type.model_class().__module__,
                            perm.content_type.model_class().__name__
                        ),
                        perm.codename
                    ) for perm in attr['user_permissions'].filter()
                ]

            # unfold group many to many relation
            if 'groups' in attr:
                attr['groups'] = [group.name for group in attr['groups'].filter()]

            return attr
        else:
            return {}
